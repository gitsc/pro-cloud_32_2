/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.admin.beans.dto.UserDTO;
import com.cloud.admin.beans.po.SysUser;
import com.cloud.common.data.base.ProMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 用户表
 *
 * @author Aijm
 * @date 2019-08-25 20:20:58
 */
public interface SysUserMapper extends ProMapper<SysUser> {

    /**
     * 分页查询
     * @param page
     * @param userDTO
     * @return
     */
    IPage<UserDTO> getSysUserPage(Page page, @Param("query") UserDTO userDTO);

    /**
     * 查询数量
     * @param userDTO
     * @return
     */
    List<Long> getCheckUser(UserDTO userDTO);

    /**
     * 查询数量  某租户的情况下
     * @param userDTO
     * @return
     */
    List<Long> getCheckUserTenant(UserDTO userDTO);


}