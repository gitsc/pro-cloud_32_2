/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.dto;

import com.cloud.admin.beans.po.SysRole;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * @Author Aijm
 * @Description 角色的详细信息
 * @Date 2019/5/13
 */
@Data
@Accessors(chain = true)
public class RoleDTO extends SysRole {

    /**
     * 拥有菜单
     */
    private List<MenuDTO> menuList;



}