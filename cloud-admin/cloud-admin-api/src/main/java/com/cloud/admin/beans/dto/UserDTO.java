/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.dto;


import com.cloud.admin.beans.po.SysMenu;
import com.cloud.admin.beans.po.SysUser;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * <p>
 * 机构表
 * </p>
 *
 * @author Aijm
 * @since 2019-05-04
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysUser对象", description="用户表")
public class UserDTO extends SysUser {


    /**
     * 所属部门
     */
    private OfficeDTO office;

    /**
     * 排序字段
     */
    private String orderBy;

    /**
     * 查询的条件
     */
    private RoleDTO role;

    /**
     * 新密码
     */
    private String newPassword;

    /**
     * 拥有的所有角色
     */
    private List<RoleDTO> roleList;

    private List<Long> roleIdList;

    private List<SysMenu> menuList;


}