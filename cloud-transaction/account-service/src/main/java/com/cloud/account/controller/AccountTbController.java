/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.account.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.account.beans.po.AccountTb;
import com.cloud.account.service.AccountTbService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 *
 *
 * @author Aijm
 * @date 2020-03-15 18:16:02
 */
@RestController
@RequestMapping("/accounttb" )
@Api(value = "accounttb", tags = "accounttb管理")
public class AccountTbController {

    @Autowired
    private AccountTbService accountTbService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param accountTb
     * @return
     */
    @GetMapping("/page")
    public Result getAccountTbPage(Page page, AccountTb accountTb) {
        return Result.success(accountTbService.page(page, Wrappers.query(accountTb)));
    }


    /**
     * 通过id查询
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(accountTbService.getById(id));
    }

    /**
     * 新增
     * @param accountTb
     * @return Result
     */
    @PostMapping
    public Result save(@RequestBody @Valid AccountTb accountTb) {
        return Result.success(accountTbService.save(accountTb));
    }

    /**
     * 修改
     * @param accountTb
     * @return Result
     */
    @PutMapping
    public Result updateById(@RequestBody @Valid AccountTb accountTb) {
        AccountTb byId = accountTbService.getById(accountTb.getId());
        byId.setMoney(byId.getMoney()-accountTb.getMoney());
        return Result.success(accountTbService.updateById(byId));
    }

    /**
     * 通过id删除
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    public Result removeById(@PathVariable Long id) {
        return Result.success(accountTbService.removeById(id));
    }

}