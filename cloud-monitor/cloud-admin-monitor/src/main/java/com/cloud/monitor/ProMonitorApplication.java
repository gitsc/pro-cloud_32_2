/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author Aijm
 * @Description admin 监控
 * 如果开启的话 请在cloud-common-data 模块中将注释spring-boot-admin-starter-client  jar 开启
 * 特别注意 不建议再生产上使用 建议使用,prometheus+grafana+alertManager
 * @Date 2019/8/6
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableAdminServer
public class ProMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProMonitorApplication.class, args);
    }
}