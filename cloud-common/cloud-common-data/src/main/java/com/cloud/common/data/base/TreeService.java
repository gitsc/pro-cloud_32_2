/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.common.data.util.TreeUtil;
import com.cloud.common.entity.TreeEntity;

/**
 * @Author Aijm
 * @Description 封装ServiceImpl
 * @Date 2019/10/11
 */
public class TreeService<M extends BaseMapper<T>, T extends TreeEntity> extends BaseService<M, T> implements IProService<T> {


    @Override
    public boolean save(T entity) {
        if (StrUtil.isBlank(entity.getParentIds())) {
            if (TreeUtil.ROOT_PID.equals(entity.getParentId())|| entity.getParentId() == null) {
                entity.setParentIds(TreeUtil.ROOT_PID+",");
                entity.setParentId(TreeUtil.ROOT_PID);
            } else {
                T t = baseMapper.selectById(entity.getParentId());
                entity.setParentIds(t.getParentIds()+t.getId()+",");
            }
        }
        return super.save(entity);
    }

}