/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.tenant;

import com.baomidou.mybatisplus.extension.plugins.tenant.TenantHandler;
import com.cloud.common.data.util.SystemUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 租户处理
 * @author Aijm
 * @since 2020/1/14
 */
@Slf4j
public class ProTenantHandler implements TenantHandler {

	@Autowired
	private ProTenantProps propes;

	/**
	 * 租户值
	 * @return
	 */
	@Override
	public Expression getTenantId(boolean where) {
		Integer tenantId = SystemUtil.getCurrentTenant();
		log.debug("当前租户的值为:{}", tenantId);
		return new LongValue(tenantId);
	}

	/**
	 * 获取租户字段名
	 *
	 * @return 租户字段名
	 */
	@Override
	public String getTenantIdColumn() {
		return propes.getColumn();
	}

	/**
	 * 根据表名判断是否进行过滤
	 * @param tableName
	 * @return
	 */
	@Override
	public boolean doTableFilter(String tableName) {
		return !propes.getTables().contains(tableName);
	}



}