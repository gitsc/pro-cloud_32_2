/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.security.util;


/**
 *  oauth 相关的key
 * @author Aijm
 * @since 2020/7/19
 */
public class OauthKeys {


    /**
     * 访问token 有效期
     */
    public static Integer ACCESS_TOKEN_VALIDITY = 10000;

    /**
     * 刷新token 有效期
     */
    public static Integer REFRESH_TOKEN_VALIDITY = 100000;

}