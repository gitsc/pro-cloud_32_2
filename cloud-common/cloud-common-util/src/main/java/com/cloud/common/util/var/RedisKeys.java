/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.util.var;

import lombok.experimental.UtilityClass;

/**
 * @Author Aijm
 * @Description 存储redis的所有key
 * @Date 2019/7/23
 */
@UtilityClass
public class RedisKeys {


    /**
     * 用来存储oauth的token
     */
    public static final String REDIS_TOKEN_KEY = "pro:redis:token:";


    ////////////////////// oauth token 校验key ///////////////////////////////////
    /**
     * 验证码code 存储key
     */
    public static final String CODE_VALID = "code:";

    /**
     * 防止用户重复提交 user:访问url: 标识token:
     */
    public static final String USER_NO_REPEAT_SUBMIT = "user:%s:%s";
}