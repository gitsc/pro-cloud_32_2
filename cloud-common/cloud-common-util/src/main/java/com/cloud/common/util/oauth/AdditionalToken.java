/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.util.oauth;

import lombok.experimental.UtilityClass;

/**
 * 扩充的token
 * @author Aijm
 * @since 2019/5/15
 */
@UtilityClass
public class AdditionalToken {


    /**
     * 制作者
     */
    public static final String MAKE_BY = "makeBy";

    public static final String MAKER = "pro-cloud";

    public static final String USER_ID = "userId";

    /**
     * 用户类型
     */
    public static final String USER_TYPE = "userType";


    /**
     * 登录的用户名
     */
    public static final String LONGIN_NAME ="user_name";


    /**
     * 管理的租户 ids
     */
    public static final String TENANT_ID ="tenantId";

}