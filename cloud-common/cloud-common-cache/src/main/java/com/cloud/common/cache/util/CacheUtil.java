/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.cache.util;

import com.cloud.common.cache.constants.CacheKeys;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;


/**
 * @Author Aijm
 * @Description  缓存工具类 建议使用 该工具类查询出来的数据不可能存在‘${NULL}’ 为null的数据
 * @Date 2019/8/29
 */
@Slf4j
@UtilityClass
public class CacheUtil extends RedisUtil{

    /**
     * 为空时的字符串
     */
    private static final String NULL_EMPTY = CacheKeys.EMPTY_OBJ;

    /**
     * 为 null 的过期时间
     */
    private static final Long EXPIRETIME_NULL = CacheKeys.EXPIRETIME_NULL;


    /**
     * 写入APPLICATION_CACHE缓存
     * @param key
     * @param value
     */
    public static void putNull(String key, Object value) {
        putNull(APPLICATION_CACHE, key, value);
    }

    /**
     * 写入缓存
     * @param cacheName
     * @param key
     * @param value
     */
    public static void putNull(String cacheName, String key, Object value) {
        if (value == null) {
            redisDao.vSet(getKey(cacheName, key), NULL_EMPTY,EXPIRETIME_NULL);
        } else {
            redisDao.vSet(getKey(cacheName, key), value);
        }
    }

    /**
     * 写入缓存 含有过期时间
     * @param cacheName
     * @param key
     * @param value
     */
    public static void putNull(String cacheName, String key, Object value, Long expireTime) {
        if (value == null) {
            redisDao.vSet(getKey(cacheName, key), NULL_EMPTY,EXPIRETIME_NULL);
        } else {
            redisDao.vSet(getKey(cacheName, key), value, expireTime);
        }
    }
}