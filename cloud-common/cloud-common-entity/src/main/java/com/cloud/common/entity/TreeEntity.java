/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * tree的基础
 * @author Aijm
 * @since 2019/5/4
 */
@Data
@Accessors(chain = true)
public class TreeEntity<T> extends BaseEntity<T> {


	@ApiModelProperty(value = "父级编号")
    protected Long parentId;

    @ApiModelProperty(value = "所有父级编号")
    protected String parentIds;

    @ApiModelProperty(value = "名称")
    protected String name;

    @ApiModelProperty(value = "排序")
    protected Integer sort;
    /**
     * 自定义SQL（SQL标识，SQL内容）
     */
    @TableField(exist = false)
    protected List<T> children;


}